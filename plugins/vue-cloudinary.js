import Vue from 'vue'
import Cloudinary from 'cloudinary-vue'
Vue.use(Cloudinary, {
  configuration: {
    cloudName: 'stonestoryby',
    fetch_format: 'auto',
    secure: true
  }
})
