module.exports = {
  mode: 'universal',
  target: 'static',
  components: true,
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      }
    ],
    script: [
      {
        src: '/replain.js'
        // defer: true
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff'
  },
  /*
   ** The layoutTransition Property
   */
  layoutTransition: 'layout',
  /*
   ** Global CSS
   */
  css: ['~/assets/css/app.css'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    /*
     ** Doc: https://www.npmjs.com/package/vue-typer
     ** Playground: https://cngu.github.io/vue-typer/#playground
     */
    {
      src: '@/plugins/vue-typer.js',
      ssr: false
    },
    { src: '@/plugins/vue-cloudinary.js' },
    {
      src: '@/plugins/vue-lazyload.js'
    },
    {
      src: '@/plugins/vue-lazysizes.client.js'
    }
  ],

  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    '@nuxtjs/pwa',
    '@nuxtjs/apollo',
    '@nuxtjs/markdownit',
    '@nuxtjs/sitemap',
    // Doc: https://www.npmjs.com/package/nuxt-webfontloader
    'nuxt-webfontloader'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  apollo: {
    clientConfigs: {
      default: {
        httpEndpoint: 'https://stonestory.herokuapp.com/graphql'
      }
    }
  },
  markdownit: {
    preset: 'default',
    // linkify: true,
    breaks: true,
    // use: ['markdown-it-div', 'markdown-it-attrs']
    injected: true
  },
  /*
   ** This option is given directly to the vue-router Router constructor
   */
  router: {
    base: '',
    linkActiveClass: 'is-active'
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** PostCSS setup
     */
    postcss: {
      // Add plugin names as key and arguments as value
      // Disable a plugin by passing false as value
      plugins: {
        cssnano: {
          preset: 'default',
          discardComments: { removeAll: true },
          zIndex: false
        }
      },
      // Change the postcss-preset-env settings
      preset: {
        autoprefixer: {
          cascade: false,
          grid: false
        }
      }
    },

    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient, loaders: { vue } }) {
      if (isClient) {
        vue.transformAssetUrls.img = ['data-src', 'src']
        vue.transformAssetUrls.source = ['data-srcset', 'srcset']
      }
    },
    /*
     ** Build config
     */
    // Сhange path for static assets
    publicPath: '/assets/',
    // Extract CSS to one file
    extractCSS: true,
    // Optimization rules
    optimization: {
      splitChunks: {
        chunks: 'async'
      }
    },
    // Add chunks
    splitChunks: {
      pages: true,
      vendor: true,
      commons: true,
      runtime: true,
      layouts: true,
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.(css|vue)$/,
          chunks: 'all',
          enforce: true
        }
      }
    },
    filenames: {
      manifest: ({ isDev }) =>
        isDev ? '[name].json' : 'bundle.[contenthash].json',
      vendor: ({ isDev }) => (isDev ? '[name].js' : 'bundle.[contenthash].js'),
      chunk: ({ isDev }) => (isDev ? '[name].js' : 'bundle.[contenthash].js'),
      app: ({ isDev }) => (isDev ? '[name].js' : 'bundle.[contenthash].js'),
      css: ({ isDev }) => (isDev ? '[name].css' : 'bundle.[contenthash].css')
    }
  },
  // Add render rules
  render: {
    bundleRenderer: {
      shouldPrefetch: (file, type) =>
        ['script', 'style', 'font'].includes(type) && !file.includes('admin')
    },
    http2: {
      push: true,
      pushAssets: (req, res, publicPath, preloadFiles) =>
        preloadFiles.map(
          f => `<${publicPath}${f.file}>; rel=preload; as=${f.asType}`
        )
    },
    compressor: false,
    resourceHints: false,
    etag: true,
    static: {
      etag: true
    }
  },
  /*
   ** Webfont Loader module configuration
   */
  webfontloader: {
    events: false,
    google: {
      families: ['Jost:400,700&display=swap&subset=cyrillic']
    },
    timeout: 5000
  },
  sitemap: {
    hostname: process.env.APP_URL,
    gzip: true,
    trailingSlash: true,
    exclude: [],
    defaults: {
      changefreq: 'daily',
      priority: 1,
      lastmod: new Date()
    }
  }
}
